var rules = {
	string : /^[A-z]{3,50}$/,
	phone : /^\d{10}$/,
	mail : /^[A-z0-9._-]+@[A-z0-9-]+\.[A-z]{2,5}$/,
	test : function (e,rule){
		if (rule.test(e.val())) return true;
		else false;
	}
}

var validControl = [false,false,false,false,false,false,false];

function isValid() {
	ctrl = true;
	for(var i = 0; i < validControl.length; i++) if (!validControl[i]) ctrl = false; 
	$("button").removeClass("btn-warning btn-success btn-danger");
 	if (ctrl) {
	    $("button").removeAttr("disabled");
	    $("button").addClass("btn-success");
 	}
	else{
	   	$("button").prop("disabled",true);
	    $("button").addClass("btn-danger");
	}
}


$(document).ready(function () {
	if ([false,false,false,false,false,false] == validControl) {alert("s")};
	$("input").on("keyup",function (e) {
		var parent = $(this).parent().parent();
    	parent.removeClass("has-error has-success");
    	parent.children(".glyphicon").removeClass("glyphicon-ok glyphicon-remove");

		var cRule = ""
		switch ($(this).data("type")){
			case "string": cRule = rules.string; break;
    		case "phone" : cRule = rules.phone ; break;
    		case "mail" : cRule = rules.mail ; break;
		}

		if (rules.test($(this),cRule)) {
			parent.addClass("has-success");
    		parent.children(".glyphicon").addClass("glyphicon-ok");
    		validControl[$(this).data("index")] = true;

    		if ($(this).data("type") == "phone" || $(this).data("type") == "mail") {
				if (Math.random() > 0.5) {
					parent.removeClass("has-error has-success");
    				parent.children(".glyphicon").removeClass("glyphicon-ok glyphicon-remove");
					parent.addClass("has-error");
				    parent.children(".glyphicon").addClass("glyphicon-remove");
			    	validControl[$(this).data("index")] = false;
				}
				/*$.ajax({ 
					url: "resources/a.txt",
					method: "GET",
					dataType: "html"
				})
				.done(function( data ) {
					if (data == true) {
						parent.removeClass("has-error has-success");
    					parent.children(".glyphicon").removeClass("glyphicon-ok glyphicon-remove");
						parent.addClass("has-error");
				    	parent.children(".glyphicon").addClass("glyphicon-remove");
			    		validControl[$(this).data("index")] = false;
			    		isValid();
					}  
				});*/
			}
		}
		else{
	    	parent.addClass("has-error");
	    	parent.children(".glyphicon").addClass("glyphicon-remove");
    		validControl[$(this).data("index")] = false;
	    }
	    isValid();
	});

	$("select").on("change", function(){
		var parent = $(this).parent().parent();
    	parent.removeClass("has-error has-success");

		if ($(this)[0].selectedIndex > 0){
			parent.addClass("has-success");	
			validControl[$(this).data("index")] = true;
		}
		else{
	    	parent.addClass("has-error");
	    	validControl[$(this).data("index")] = false;
	    }
	    isValid();
	});

	$(".dbValid").on("keyup", function(){

	});

});
